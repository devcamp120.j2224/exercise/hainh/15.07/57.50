public class Account {
    int id;
    Customer customer;
    double balace = 0.0;
    
    public Account(int id, Customer customer, double balace) {
        this.id = id;
        this.customer = customer;
        this.balace = balace;
    }

    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getBalace() {
        return balace;
    }

    public void setBalace(double balace) {
        this.balace = balace;
    }

    @Override
    public String toString() {
        double roundOff = (double) Math.round(balace * 100) / 100;
        return String.format("Name (id = " + id +  ")" + ", balace = " + roundOff +")" );
    }

    public String getCustomerName(){
        return customer.getName();
    }

    public Account deposit (double amount){
        this.balace += amount;
        return this;
    }

    public Account withdraw (double amount){
        if(this.balace >= amount){
            this.balace -= amount;
        }else{
            System.out.println("Amout withdrawn exceeds the current balance!");
        }
        return this;
    }
}
